export interface ToDoItem {
  id: string;
  date: Date;
  description: string;
  done: boolean;
}
