import { nanoid } from 'nanoid';
import { ToDoItem } from './todo-item';

export const todos: ToDoItem[] = [
  {
    id: nanoid(),
    date: new Date(),
    description: 'create to-do app',
    done: false,
  },
  {
    id: nanoid(),
    date: new Date(),
    description: 'drink water',
    done: false,
  },
  {
    id: nanoid(),
    date: new Date(),
    description: 'clean kitchen',
    done: false,
  },
  {
    id: nanoid(),
    date: new Date(),
    description: 'listen music',
    done: false,
  },
];
