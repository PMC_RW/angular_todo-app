import { Component } from '@angular/core';

import { ToDoItem } from '../todo-item';
import { todos } from '../mock-todos';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css'],
})
export class TodoListComponent {
  todos: ToDoItem[] = todos;
}
