import { Component } from '@angular/core';
import { nanoid } from 'nanoid';

import { ToDoItem } from '../todo-item';
import { todos } from '../mock-todos';

@Component({
  selector: 'app-create-todo',
  templateUrl: './create-todo.component.html',
  styleUrls: ['./create-todo.component.css'],
})
export class CreateTodoComponent {
  description = '';

  todo: ToDoItem = {
    id: nanoid(),
    date: new Date(),
    description: '',
    done: false,
  };

  saveToDo(): void {
    if (this.description.length >= 5) {
      this.todo.description = this.description;
      todos.push(this.todo);
    }
  }
}
